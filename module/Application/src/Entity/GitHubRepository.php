<?php

namespace Application\Entity;

class GitHubRepository
{
    private $userName;

    private $repositoryName;

    private $forks;

    private $stars;

    private $watchers;

    private $latestReleaseDate;

    private $openPullRequests;

    private $closedPullRequests;

    public function __construct(
        int $forks,
        int $stars,
        int $watchers,
        \DateTime $latestReleaseDate,
        int $openPullRequests,
        int $closedPullRequests,
        string $repositoryName,
        string $userName
    ) {
        $this->forks = $forks;
        $this->stars = $stars;
        $this->watchers = $watchers;
        $this->latestReleaseDate = $latestReleaseDate;
        $this->openPullRequests = $openPullRequests;
        $this->closedPullRequests = $closedPullRequests;
        $this->repositoryName = $repositoryName;
        $this->userName = $userName;
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function getRepositoryName(): string
    {
        return $this->repositoryName;
    }

    public function getForks(): int
    {
        return $this->forks;
    }

    public function getStars(): int
    {
        return $this->stars;
    }

    public function getWatchers(): int
    {
        return $this->watchers;
    }

    public function getLatestReleaseDate(): \DateTime
    {
        return $this->latestReleaseDate;
    }

    public function getOpenPullRequests(): int
    {
        return $this->openPullRequests;
    }

    public function getClosedPullRequests(): int
    {
        return $this->closedPullRequests;
    }
}
