<?php

namespace Application\Entity;

class GitHubRepositoryNames
{
    private $userName;

    private $repositoryName;

    public function __construct(string $repositoryName, ?string $userName = null)
    {
        $this->userName = $userName;
        $this->repositoryName = $repositoryName;
    }

    public function concatNames(): string
    {
        return ($this->userName ? ($this->userName . '/') : '') . $this->repositoryName;
    }
}
