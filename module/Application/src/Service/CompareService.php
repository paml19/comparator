<?php

namespace Application\Service;

use Application\Entity\GitHubRepository;

class CompareService
{
    private $gitHubService;

    private $gitHubRepositorySerializer;

    public function __construct(GitHubService $gitHubService, GitHubRepositorySerializer $gitHubRepositorySerializer)
    {
        $this->gitHubService = $gitHubService;
        $this->gitHubRepositorySerializer = $gitHubRepositorySerializer;
    }

    public function compareAll(RouteParams $routeParams): array
    {
        return array_map(function (GitHubRepository $repository) {
            return $this->gitHubRepositorySerializer->serialize($repository);
        }, $this->getRepositories($routeParams));
    }

    private function getRepositories(RouteParams $routeParams): array
    {
        return [
            'based' => $this->gitHubService->getRepository($routeParams->getBased()),
            'compared' => $this->gitHubService->getRepository($routeParams->getCompared())
        ];
    }
}
