<?php

namespace Application\Service;

use Application\Entity\GitHubRepository;
use Github\Client;

class GitHubService
{
    private $client;

    private $gitHubParser;

    public function __construct(
        Client $client,
        GitHubParser $gitHubParser
    ) {
        $this->client = $client;
        $this->gitHubParser = $gitHubParser;
    }

    public function getRepository(string $name): GitHubRepository
    {
        $repositoryNames = $this->gitHubParser->parse($name);
        $repositorySmallData = $this->client->api('repo')->find($repositoryNames->concatNames())['repositories'];

        if (! isset($repositorySmallData[0])) {
            throw new \Exception('Repository not exists');
        }

        $repositorySmallData = $repositorySmallData[0];
        $repositoryBigData = $this->client->repo()->show($repositorySmallData['username'], $repositorySmallData['name']);

        $lastRelease = $this->client->repo()->releases()->latest(
            $repositorySmallData['username'],
            $repositorySmallData['name']
        );

        $repository = new GitHubRepository(
            $repositoryBigData['forks'],
            $repositoryBigData['stargazers_count'],
            $repositoryBigData['subscribers_count'],
            new \DateTime($lastRelease['published_at']),
            count($this->client->pullRequest()->all($repositorySmallData['username'], $repositorySmallData['name'], ['state' => 'open'])),
            count($this->client->pullRequest()->all($repositorySmallData['username'], $repositorySmallData['name'], ['state' => 'closed'])),
            $repositorySmallData['name'],
            $repositorySmallData['username']
        );

        return $repository;
    }
}
