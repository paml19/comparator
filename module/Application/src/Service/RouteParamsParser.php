<?php

namespace Application\Service;

class RouteParamsParser
{
    public function parse(array $routeParams): RouteParams
    {
        return new RouteParams($routeParams['based'], $routeParams['compared']);
    }
}