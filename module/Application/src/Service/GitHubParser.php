<?php

namespace Application\Service;

use Application\Entity\GitHubRepositoryNames;

class GitHubParser
{
    public function parse(string $repository): GitHubRepositoryNames
    {
        $explodedName = explode('/', $repository);
        $gitHubRepository = null;

        $newExplodedName = [];
        foreach ($explodedName as $item) {
            if ($item != '') {
                $newExplodedName[] = $item;
            }
        }

        $explodedNameCount = count($newExplodedName);

        switch ($explodedNameCount) {
            case 1:
                $gitHubRepository = new GitHubRepositoryNames($newExplodedName[0]);
                break;
            case 2:
                $gitHubRepository = new GitHubRepositoryNames($newExplodedName[1], $newExplodedName[0]);
                break;
            default:
                $gitHubRepository = new GitHubRepositoryNames(
                    $newExplodedName[$explodedNameCount - 2],
                    $newExplodedName[$explodedNameCount - 1]
                );
                break;
        }

        return $gitHubRepository;
    }
}
