<?php

namespace Application\Service;

class RouteParams
{
    private $based;

    private $compared;

    public function __construct(string $based, string $compared)
    {
        $this->based = $based;
        $this->compared = $compared;
    }

    public function getBased(): string
    {
        return $this->based;
    }

    public function getCompared(): string
    {
        return $this->compared;
    }
}