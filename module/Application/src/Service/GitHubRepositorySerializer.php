<?php

namespace Application\Service;

use Application\Entity\GitHubRepository;

class GitHubRepositorySerializer
{
    public function serialize(GitHubRepository $gitHubRepository): array
    {
        return [
            'user' => $gitHubRepository->getUserName(),
            'repository' => $gitHubRepository->getRepositoryName(),
            'forks' => $gitHubRepository->getForks(),
            'stars' => $gitHubRepository->getStars(),
            'watchers' => $gitHubRepository->getWatchers(),
            'lastReleaseDate' => $gitHubRepository->getLatestReleaseDate()->format('d.m.Y H:i:s'),
            'openPullRequests' => $gitHubRepository->getOpenPullRequests(),
            'closedPullRequests' => $gitHubRepository->getClosedPullRequests(),
            '__links' => [
                'self' => sprintf('/api/v1/repositories/%s/%s',
                    $gitHubRepository->getUserName(),
                    $gitHubRepository->getRepositoryName()
                ),
            ]
        ];
    }
}
