<?php

namespace Application\Controller\V1;

use Application\Service\GitHubRepositorySerializer;
use Application\Service\GitHubService;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class RepositoryController extends AbstractRestfulController
{
    private $gitHubService;

    private $gitHubRepositorySerializer;

    public function __construct(GitHubService $gitHubService, GitHubRepositorySerializer $gitHubRepositorySerializer)
    {
        $this->gitHubService = $gitHubService;
        $this->gitHubRepositorySerializer = $gitHubRepositorySerializer;
    }

    public function get($id)
    {
        try {
            $response = $this->gitHubRepositorySerializer->serialize($this->gitHubService->getRepository($id));
        } catch (\Exception $exception) {
            $this->response->setStatusCode(Response::STATUS_CODE_404);

            $response = [
                'error' => $exception->getMessage(),
                'code' => $exception->getCode()
            ];
        }

        return new JsonModel($response);
    }
}
