<?php

namespace Application\Controller\V1;

use Application\Service\CompareService;
use Application\Service\RouteParamsParser;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class CompareController extends AbstractRestfulController
{
    private $compareService;

    private $routeParamsParser;

    public function __construct(CompareService $compareService, RouteParamsParser $routeParamsParser)
    {
        $this->compareService = $compareService;
        $this->routeParamsParser = $routeParamsParser;
    }

    public function getList()
    {
        $routeParams = $this->routeParamsParser->parse($this->params()->fromRoute());

        try {
            $response = $this->compareService->compareAll($routeParams);
        } catch (\Exception $exception) {
            $this->response->setStatusCode(Response::STATUS_CODE_404);

            $response = [
                'error' => $exception->getMessage(),
                'code' => $exception->getCode()
            ];
        }

        return new JsonModel($response);
    }
}
