<?php

namespace Application;

use Github\Client;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Regex;
use Zend\ServiceManager\AbstractFactory\ReflectionBasedAbstractFactory;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\V1\RepositoryController::class => ReflectionBasedAbstractFactory::class,
            Controller\V1\CompareController::class => ReflectionBasedAbstractFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'api' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api',
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'v1' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/v1',
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'repository' => [
                                'type' => Regex::class,
                                'options' => [
                                    'regex' => '/repositories(?<id>\/.*)',
                                    'spec' => '/id%',
                                    'defaults' => [
                                        'controller' => Controller\V1\RepositoryController::class,
                                    ],
                                ],
                            ],
                            'compare' => [
                                'type' => Regex::class,
                                'options' => [
                                    'regex' => '/compare(?<based>\/.*)/to(?<compared>\/.*)',
                                    'spec' => '/compare%based%/to%compared%',
                                    'defaults' => [
                                        'controller' => Controller\V1\CompareController::class,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\GitHubService::class => ReflectionBasedAbstractFactory::class,
            Service\GitHubParser::class => InvokableFactory::class,
            Service\CompareService::class => ReflectionBasedAbstractFactory::class,
            Service\RouteParamsParser::class => InvokableFactory::class,
            Service\GitHubRepositorySerializer::class => InvokableFactory::class,
        ],
        'invokables' => [
            Client::class => Client::class,
        ],
    ],
    'view_manager' => [
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
