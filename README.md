# Comparator
It is a simple version of comparator project, which shows any statistics from GitHub repositories and compare with
 the other.
___
## Install
1. Make sure you've got a PHP 7.2,
2. Clone repository to yours machine,
3. Run `php composer.phar install`,
4. Run `php composer.phar serve` and enjoy.
---
## Usage
Application have got just to endpoints wih method HTTP GET:
+ `/api/v1/repositories/{repository name or url}` to get repository data
+ `/api/v1/compare/{repository name or url one}/to/{repository name or url two}` to get repositories data compared
 each other